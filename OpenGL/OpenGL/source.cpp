#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "GL/glut.h"

#define Size 250

float cameraScale = 0.005;
float global_scale = 0.005;
float house_scale = 0.005;
float tree_scale = 0.1;
float sol_scale = 0.00001;
float umbreon_scale = 0.1;
float movementScale = 0.1;
float groudon_scale = 0.01;
int dimensionSize = 3;
float cameraParam = -1;

class Vertex
{
public:
	bool colored;
	int color[3];
	std::vector<float> coordinate;
	std::vector<float> normal_x;
	std::vector<float> normal_y;
	std::vector<float> normal_z;
	std::vector<float> normalComb;

	Vertex(float x, float y, float z)
	{
		coordinate.push_back(x);
		coordinate.push_back(y);
		coordinate.push_back(z);

		normalComb.push_back(0);
		normalComb.push_back(0);
		normalComb.push_back(0);

		for (int i = 0; i < 3; i++)
		{
			color[i] = 0;
		}

		colored = false;
	}
	Vertex()
	{
		for (int i = 0; i < 3; i++)
		{
			color[i] = 0;
		}

		colored = false;
	}
	void Reset()
	{
		for (int i = 0; i < coordinate.size(); i++)
		{
			coordinate[i] = 0;
		}
	}

	void Initialize(float x, float y, float z)
	{
		coordinate.push_back(x);
		coordinate.push_back(y);
		coordinate.push_back(z);
	}

	void Set(float x, float y, float z)
	{
		if (coordinate.size() == 3)
		{
			coordinate[0] = x;
			coordinate[1] = y;
			coordinate[2] = z;
		}
		else
		{
			coordinate.push_back(x);
			coordinate.push_back(y);
			coordinate.push_back(z);
		}
	}

	void SetColor(int r, int g, int b)
	{
		color[0] += r;
		color[1] += g;
		color[2] += b;
		if (colored)
		{
			for(int i = 0; i < 3; i++)
			{
				color[i] /= 2;
			}
		}
		colored = true;
	}

	void Set(Vertex &another)
	{
		for (int i = 0; i < coordinate.size(); i++)
		{
			coordinate[i] = another.coordinate[i];
		}
	}

	void AddNormal(float x, float y, float z)
	{
		normal_x.push_back(x);
		normal_y.push_back(y);
		normal_z.push_back(z);
	}

	void ClearNormal()
	{
		normal_x.clear();
		normal_y.clear();
		normal_z.clear();
	}

	Vertex GetNormal(int index)
	{
		return Vertex(normal_x[index], normal_y[index], normal_z[index]);
	}

	int GetNormalSize()
	{
		return normal_x.size();
	}

	void SetNormalComb(float x, float y, float z)
	{
		normalComb[0] = x;
		normalComb[1] = y;
		normalComb[2] = z;
	}

	float GetLength()
	{
		float total = 0;
		for (int i = 0; i < coordinate.size(); i++)
		{
			total += (float)pow(coordinate[i], (float)2.0);
		}
		total = (float)pow((float)total, (float)0.5);
		return total;
	}
	
};

class Camera
{
public:
	Vertex position;
	Vertex rotation;
	Vertex light;
	Camera(float x = 0, float y = 0, float z = 0, float rotate_x = 0, float rotate_y = 0, float rotate_z = 0)
	{
		position.coordinate.push_back(x);
		position.coordinate.push_back(y);
		position.coordinate.push_back(z);

		rotation.coordinate.push_back(rotate_x);
		rotation.coordinate.push_back(rotate_y);
		rotation.coordinate.push_back(rotate_z);

		light.coordinate.push_back(-2);
		light.coordinate.push_back(1);
		light.coordinate.push_back(1);
	}

	void Translate(float x, float y, float z)
	{
		position.coordinate[0] += x;
		position.coordinate[1] += y;
		position.coordinate[2] += z;
	}

	void Rotate(float rotate_x, float rotate_y, float rotate_z)
	{
		rotation.coordinate[0] += rotate_x * 22 / (7 * 180);
		rotation.coordinate[1] += rotate_y * 22 / (7 * 180);
		rotation.coordinate[2] += rotate_z * 22 / (7 * 180);

		for (int i = 0; i < rotation.coordinate.size(); i++)
		{
			if (rotation.coordinate[i] <= -360.0)
			{
				rotation.coordinate[i] += 360.0;
			}
			else if (rotation.coordinate[i] >= 360.0)
			{
				rotation.coordinate[i] -= 360.0;
			}
		}
	}

	void UpdateLight(Vertex sol_position)
	{
		light.coordinate[0] = sol_position.coordinate[0];
		light.coordinate[1] = sol_position.coordinate[1];
		light.coordinate[2] = sol_position.coordinate[2];
	}
};

class Face
{
public:
	std::vector<std::vector<Vertex>> vcomp;
	Vertex color;
	Face()
	{
		color.Set(0, 0, 0);
	}

	void SetColor(int r, int g, int b)
	{
		color.Set(r, g, b);
	}
	//std::vector<Vertex>* GetVM(int index)
	//{
	//	return (index == 0)? &v : (index == 1)? &vt : &vn;
	//}
};

void AddVertex(Vertex &a, Vertex &b)
{
	for (int i = 0; i < a.coordinate.size(); i++)
	{
		a.coordinate[i] += b.coordinate[i];
	}
}

void DivideVertex(Vertex &a, float &b)
{
	for (int i = 0; i < a.coordinate.size(); i++)
	{
		a.coordinate[i] /= b;
	}
}

float DotVertex(Vertex &a, Vertex &b)
{
	float total = 0;
	for (int i = 0; i < a.coordinate.size(); i++)
	{
		total += a.coordinate[i] * b.coordinate[i];
	}
	return total;
}

void MultiplyVertex(Vertex &a, float multiplier)
{
	for (int i = 0; i < a.coordinate.size(); i++)
	{
		a.coordinate[i] *= multiplier;
	}
}

void SubstractVertex(Vertex &result, Vertex &a, Vertex &b)
{
	for (int i = 0; i < a.coordinate.size(); i++)
	{
		result.coordinate[i] = a.coordinate[i] - b.coordinate[i];
	}
}

void Normalize(Vertex &a)
{
	float manhattan = 0;
	for (int i = 0; i < a.coordinate.size(); i++)
	{
		manhattan += (float)pow(a.coordinate[i], (float)2.0);
	}
	manhattan = (float)pow(manhattan, (float)0.5);
	for (int i = 0; i < a.coordinate.size(); i++)
	{
		a.coordinate[i] /= manhattan;
	}
}

float GetCosine(Vertex &a, Vertex &b)
{
	float total = 0;
	for (int i = 0; i < a.coordinate.size(); i++)
	{
		total += a.coordinate[i] * b.coordinate[i];
	}
	//std::cout << total << "\n";
	return total;
}

void GetNormal(Vertex &normal, std::vector<Vertex> &pts)
{
	Vertex line1(0, 0, 0), line2(0, 0, 0), temp(0, 0, 0);
	
	for (int i = 0; i < line1.coordinate.size(); i++)
	{
		line1.coordinate[i] = pts[1].coordinate[i] - pts[0].coordinate[i];
	}
	
	for (int i = 0; i < line2.coordinate.size(); i++)
	{
		line2.coordinate[i] = pts[2].coordinate[i] - pts[0].coordinate[i];
	}

	normal.coordinate[0] = (line1.coordinate[1] * line2.coordinate[2]) - (line1.coordinate[2] * line2.coordinate[1]);
	normal.coordinate[1] = (-1.0) * ((line1.coordinate[0] * line2.coordinate[2]) - (line1.coordinate[2] * line2.coordinate[0]));
	normal.coordinate[2] = (line1.coordinate[0] * line2.coordinate[1]) - (line1.coordinate[1] * line2.coordinate[0]);
}

void GetMidpoint(Vertex &midpoint, std::vector<Vertex> &pts)
{
	midpoint.Reset();
	for (int i = 0; i < pts.size(); i++)
	{
		for (int j = 0; j < pts[i].coordinate.size(); j++)
		{
			midpoint.coordinate[j] += (pts[i].coordinate[j] / pts.size());
		}
	}
}

class Mesh
{
	float x, y, z;
	float local_scale;
	std::vector<float> color;
	std::vector<Face> faces;
	std::vector<Vertex> vlib;
	std::vector<Vertex> vtlib;
	std::vector<Vertex> vnlib;
	std::vector<Vertex> normals;
	std::vector<Vertex> pts;
	std::vector<Vertex> textures;
	std::vector<std::vector<Vertex>> ptsArr;
	std::vector<std::vector<Vertex>> texturesArr;
	std::vector<std::vector<Vertex>> modifiedPts;
	std::vector<std::vector<Vertex>> modifiedTextures;
	Vertex normal, light_direction, midpoint, light_point;
public:
	Mesh(float _x = 0, float _y = 0, float _z = 0)
	{
		x = _x;
		y = _y;
		z = _z;
		color.push_back(182.0 / 255.0);
		color.push_back(154.0 / 255.0);
		color.push_back(128.0 / 255.0);
		
		for (int i = 0; i < 4; i ++)
		{
			//normals.push_back(Vertex(0, 0, 0));
			pts.push_back(Vertex(0, 0, 0));
			//textures.push_back(Vertex(0, 0, 0));
		}

		normal.Initialize(0, 0, 0);
		light_direction.Initialize(0, 0, 0);
		midpoint.Initialize(0, 0, 0);
		light_point.Initialize(0, 0, 2);
	}

	void SetScale(float _scale)
	{
		local_scale = _scale;
	}

	void LoadFromFile(std::string filename)
	{
		Vertex point;
		std::fstream file;
		std::string temp, mode;
		file.open(filename, std::ios_base::in);
		
		file >> temp;
		int count = 0;
		while(temp != "g")
		{
			//std::cout << temp << "\n";
			point.coordinate.clear();
			if (temp == "v" || temp == "vt" || temp == "vn")
			{
				mode = temp;
				file >> temp;
				while (temp != "v" && temp != "vt" && temp != "vn" && temp != "#")
				{
					//std::cout << temp << "\n";
					point.coordinate.push_back(std::stof(temp));
					file >> temp;
				}

				if (mode == "v")
				{
					vlib.push_back(point);
				}
				else if(mode == "vt")
				{
					vtlib.push_back(point);
				}
				else if(mode == "vn")
				{
					vnlib.push_back(point);
				}
			}
			else
			{
				file >> temp;
			}
		}
		std::cout << "Begin on Face..\n";
		Vertex fcolor(0, 0, 0);
		file >> temp;
		//std::cout << temp << "\n";
		while(temp != "g")
		{
			//std::cout << temp << "\n";
			if (temp == "f")
			{
				faces.push_back(Face());
				file >> temp;
				while(temp != "f" && temp != "g" && temp != "#" && temp != "usemtl")
				{
					faces[faces.size() - 1].vcomp.push_back(std::vector<Vertex>());
					point.coordinate.clear();
					while(temp.find("/") != std::string::npos)
					{
						point.coordinate.push_back(std::stoi(temp.substr(0, temp.find("/"))));
						temp.erase(0, temp.find("/") + 1);
					}
					if (temp != "")
					{
						point.coordinate.push_back(std::stoi(temp));
					}
					faces[faces.size() - 1].vcomp[faces[faces.size() - 1].vcomp.size() - 1].push_back(point);
					file >> temp;
				}
				faces[faces.size() - 1].SetColor(fcolor.coordinate[0], fcolor.coordinate[1], fcolor.coordinate[2]);
			}
			else if(temp == "usemtl")
			{
				file >> temp;
				for (int i = 0; i < 3; i++)
				{
					fcolor.coordinate[i] = std::stoi(temp.substr(0, temp.find("/")));
					temp.erase(0, temp.find("/") + 1);
				}
			}
			else
			{
				file >> temp;
			}
		}
		std::cout << "Face Done!\n";
	}

	void Display()
	{
		for (int i = 0; i < vlib.size(); i++)
		{
			std::cout << "v" << (i + 1) << " ";
			for (int j = 0; j < vlib[i].coordinate.size(); j++)
			{
				std::cout << vlib[i].coordinate[j] << " ";
			}
			std::cout << "\n";
		}

		for (int i = 0; i < vtlib.size(); i++)
		{
			std::cout << "vt" << (i + 1) << " ";
			for (int j = 0; j < vtlib[i].coordinate.size(); j++)
			{
				std::cout << vtlib[i].coordinate[j] << " ";
			}
			std::cout << "\n";
		}

		for (int i = 0; i < vnlib.size(); i++)
		{
			std::cout << "vn" << (i + 1) << " ";
			for (int j = 0; j < vnlib[i].coordinate.size(); j++)
			{
				std::cout << vnlib[i].coordinate[j] << " ";
			}
			std::cout << "\n";
		}

		for(int i = 0; i < faces.size(); i++)
		{
			for(int j = 0; j < faces[i].vcomp.size(); j++)
			{
				std::cout << "Face " << (i + 1) << " - " << (j + 1) << "\t";
				for(int k = 0; k < faces[i].vcomp[j].size(); k++)
				{
					for(int l = 0; l < faces[i].vcomp[j][k].coordinate.size(); l++)
					{
						std::cout << faces[i].vcomp[j][k].coordinate[l] << " ";
					}
					std::cout << "\t";
				}
				std::cout << "\n";
			}
		}
	}

	void GouraudShading(Camera &camera)
	{
		Vertex normalComb(0, 0, 0);
		Vertex fcolor(0, 0, 0);
		Vertex reflection(0, 0, 0);
		Vertex modifiedNormal(0, 0, 0);
		Vertex eyeVector(0, 0, 0);
		light_point.Set(camera.light);
		float colorMultiplier, specularMultiplier;
		float distance;
		for (int i = 0; i < faces.size(); i++)
		{
			glBegin(GL_POLYGON);
			for (int j = 0; j < faces[i].vcomp.size(); j++)
			{
				normalComb.Reset();
				for (int k = 0; k < faces[i].vcomp[j].size(); k++)
				{
					normalComb.Set(modifiedPts[i][j].normalComb[0], modifiedPts[i][j].normalComb[1], modifiedPts[i][j].normalComb[2]);
					SubstractVertex(light_direction, modifiedPts[i][j], light_point);
					//SubstractVertex(light_direction, light_point, modifiedPts[i][j]);

					modifiedNormal.Set(normalComb);
					MultiplyVertex(modifiedNormal, DotVertex(light_direction, normalComb) * 2);
					SubstractVertex(reflection, light_direction, modifiedNormal);
					SubstractVertex(eyeVector, camera.position, light_point);
					Normalize(reflection);
					Normalize(eyeVector);
					specularMultiplier = GetCosine(reflection, eyeVector);
					specularMultiplier = (specularMultiplier < 0) ? (-1) * specularMultiplier : specularMultiplier;

					distance = (light_direction.GetLength());
					Normalize(light_direction);
					Normalize(normalComb);
					colorMultiplier = GetCosine(light_direction, normalComb);
					colorMultiplier = (colorMultiplier < 0) ? (-1) * colorMultiplier : colorMultiplier;
					colorMultiplier /= pow(distance, (float)0.1);
					
					specularMultiplier /= pow(distance, (float)0.1);
					//colorMultiplier += specularMultiplier;

					fcolor.Set((float)modifiedPts[i][j].color[0] / 255.0, (float)modifiedPts[i][j].color[1] / 255.0, (float)modifiedPts[i][j].color[2] / 255.0);
					//std::cout << modifiedPts[i][j].color[0] << ", " << modifiedPts[i][j].color[1] << ", " << modifiedPts[i][j].color[2] << "\n";
					//std::cout << fcolor.coordinate[0] << ", " << fcolor.coordinate[1] << ", " << fcolor.coordinate[2] << "\n";
					glColor3f(fcolor.coordinate[0] * colorMultiplier, fcolor.coordinate[1] * colorMultiplier, fcolor.coordinate[2] * colorMultiplier);
					//glTexCoord3f(modifiedTextures[i][j].coordinate[0], modifiedTextures[i][j].coordinate[1], modifiedTextures[i][j].coordinate[2]);
					glVertex3f(modifiedPts[i][j].coordinate[0], modifiedPts[i][j].coordinate[1], modifiedPts[i][j].coordinate[2]);
				}
			}
			glEnd();
		}
	}

	void FlatShading(Camera &camera)
	{
		Vertex normalComb(0, 0, 0);
		light_point.Set(camera.light);
		float colorMultiplier;
		for (int i = 0; i < faces.size(); i++)
		{
			normalComb.Set(normals[i].coordinate[0], normals[i].coordinate[1], normals[i].coordinate[2]);
			Normalize(normalComb);
			glBegin(GL_POLYGON);
			for (int j = 0; j < faces[i].vcomp.size(); j++)
			{
				for (int k = 0; k < faces[i].vcomp[j].size(); k++)
				{
					SubstractVertex(light_direction, light_point, modifiedPts[i][j]);
					Normalize(light_direction);
					colorMultiplier = GetCosine(light_direction, normalComb);
					//colorMultiplier = (colorMultiplier < 0) ? (-1) * colorMultiplier : colorMultiplier;
					glColor3f(color[0] * colorMultiplier, color[1] * colorMultiplier, color[2] * colorMultiplier);
					glVertex3f(modifiedPts[i][j].coordinate[0], modifiedPts[i][j].coordinate[1], modifiedPts[i][j].coordinate[2]);
				}
			}
			glEnd();
		}
	}

	void CalculatePosition(Camera &camera)
	{
		float scale = local_scale;
		std::cout << "Initializing Points..\n";
		Vertex normalComb(0, 0, 0);

		float temp_x = 0, temp_y = 0, temp_z = 0;
		//float temp_x = x - camera.position.coordinate[0], temp_y = y - camera.position.coordinate[1], temp_z = z - camera.position.coordinate[2];
		float x_rot_1 = cos(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[2]);
		float x_rot_2 = cos(camera.rotation.coordinate[1]) * sin(camera.rotation.coordinate[2]);
		float x_rot_3 = sin(camera.rotation.coordinate[1]);

		float y_rot_1 = ((sin(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[2])) - (cos(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[2])));
		float y_rot_2 = ((sin(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * sin(camera.rotation.coordinate[2])) + (cos(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[2])));
		float y_rot_3 = (sin(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[1]));

		float z_rot_1 = ((cos(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[2])) + (sin(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[2])));
		float z_rot_2 = ((cos(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * sin(camera.rotation.coordinate[2])) - (sin(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[2])));
		float z_rot_3 = (cos(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[1]));

		float colorMultiplier;

		light_point.Set(camera.light.coordinate[0], camera.light.coordinate[1], camera.light.coordinate[2]);

		for (int i = 0; i < vlib.size(); i++)
		{
			vlib[i].ClearNormal();
		}
		std::cout << "Calculating Faces..\n";
		for (int i = 0; i < faces.size(); i++)
		{
			pts.clear();
			float _x, _y, _z, _x_r, _y_r, _z_r;
			float tx, ty, tz, tx_r, ty_r, tz_r;
			float nx, ny, nz, nx_r, ny_r, nz_r;
			//std::cout << "Calculating Vertex..\n";
			for (int j = 0; j < faces[i].vcomp.size(); j++)
			{
				pts.push_back(Vertex(0, 0, 0));
				for (int k = 0; k < faces[i].vcomp[j].size(); k++)
				{
					_x = vlib[faces[i].vcomp[j][k].coordinate[0] - 1].coordinate[0] * scale + temp_x;
					_y = vlib[faces[i].vcomp[j][k].coordinate[0] - 1].coordinate[1] * scale + temp_y;
					_z = vlib[faces[i].vcomp[j][k].coordinate[0] - 1].coordinate[2] * scale + temp_z;

					_x_r = (x_rot_1 * _x) + (x_rot_2 * _y) - (x_rot_3 * _z);
					_y_r = y_rot_1 * _x + y_rot_2 * _y + y_rot_3 * _z;
					_z_r = z_rot_1 * _x + z_rot_2 * _y + z_rot_3 * _z;

					/*tx = vtlib[faces[i].vcomp[j][k].coordinate[1] - 1].coordinate[0] * scale + temp_x;
					ty = vtlib[faces[i].vcomp[j][k].coordinate[1] - 1].coordinate[1] * scale + temp_y;
					tz = vtlib[faces[i].vcomp[j][k].coordinate[1] - 1].coordinate[2] * scale + temp_z;

					tx_r = (x_rot_1 * tx) + (x_rot_2 * ty) - (x_rot_3 * tz);
					ty_r = y_rot_1 * tx + y_rot_2 * ty + y_rot_3 * tz;
					tz_r = z_rot_1 * tx + z_rot_2 * ty + z_rot_3 * tz;

					textures[j].coordinate[0] = tx_r;
					textures[j].coordinate[1] = ty_r;
					textures[j].coordinate[2] = tz_r;*/

					pts[j].coordinate[0] = _x_r;
					pts[j].coordinate[1] = _y_r;
					pts[j].coordinate[2] = _z_r;

					pts[j].SetColor(faces[i].color.coordinate[0], faces[i].color.coordinate[1], faces[i].color.coordinate[2]);
					//std::cout << faces[i].color.coordinate[0] << ", " << faces[i].color.coordinate[1] << ", " << faces[i].color.coordinate[2] << "\n";
				}
			}
			//std::cout << "Vertex Done!\n";
			GetNormal(normal, pts);
			Normalize(normal);

			if (ptsArr.size() < faces.size())
			{
				ptsArr.push_back(pts);
				//texturesArr.push_back(textures);
			}
			else
			{
				for (int m = 0; m < ptsArr[i].size(); m++)
				{
					ptsArr[i][m].Set(pts[m]);
					//texturesArr[i][m].Set(textures[m]);
				}
			}
			normals.push_back(normal);
			for (int j = 0; j < faces[i].vcomp.size(); j++)
			{
				for (int k = 0; k < faces[i].vcomp[j].size(); k++)
				{
					vlib[faces[i].vcomp[j][k].coordinate[0] - 1].AddNormal(normal.coordinate[0], normal.coordinate[1], normal.coordinate[2]);
				}
			}
		}
		std::cout << "Face Calculation Done!\n";
		temp_x = x - camera.position.coordinate[0], temp_y = y - camera.position.coordinate[1], temp_z = z - camera.position.coordinate[2];
		for (int i = 0; i < faces.size(); i++)
		{
			for (int j = 0; j < faces[i].vcomp.size(); j++)
			{
				normalComb.Reset();
				int count = 1;
				//std::cout << faces[i].vcomp[j].size() << "\n";
				for (int k = 0; k < faces[i].vcomp[j].size(); k++)
				{
					for (int l = 0; l < vlib[faces[i].vcomp[j][k].coordinate[0] - 1].GetNormalSize(); l++)
					{
						AddVertex(normalComb, vlib[faces[i].vcomp[j][k].coordinate[0] - 1].GetNormal(l));
						//count++;
					}
				}
				ptsArr[i][j].SetNormalComb(normalComb.coordinate[0] / count, normalComb.coordinate[1] / count, normalComb.coordinate[2] / count);
			}
			modifiedPts.push_back(ptsArr[i]);
			for (int j = 0; j < modifiedPts[i].size(); j++)
			{
				modifiedPts[i][j].coordinate[0] += temp_x;
				modifiedPts[i][j].coordinate[1] += temp_y;
				modifiedPts[i][j].coordinate[2] += temp_z;
			}
			//modifiedTextures.push_back(texturesArr[i]);
		}
		std::cout << "Initialization Done!\n";
	}

	void Update(Camera &camera)
	{
		float _x, _y, _z, _x_r, _y_r, _z_r;
		float tx, ty, tz, tx_r, ty_r, tz_r;
		float nx, ny, nz, nx_r, ny_r, nz_r;
		float mx, my, mz;

		mx = x + (-1.0) * camera.position.coordinate[0];
		my = y + (-1.0) * camera.position.coordinate[1];
		mz = z + (-1.0) * camera.position.coordinate[2];
		
		float x_rot_1 = cos(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[2]);
		float x_rot_2 = cos(camera.rotation.coordinate[1]) * sin(camera.rotation.coordinate[2]);
		float x_rot_3 = sin(camera.rotation.coordinate[1]);

		float y_rot_1 = ((sin(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[2])) - (cos(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[2])));
		float y_rot_2 = ((sin(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * sin(camera.rotation.coordinate[2])) + (cos(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[2])));
		float y_rot_3 = (sin(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[1]));

		float z_rot_1 = ((cos(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[2])) + (sin(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[2])));
		float z_rot_2 = ((cos(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * sin(camera.rotation.coordinate[2])) - (sin(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[2])));
		float z_rot_3 = (cos(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[1]));

		for (int i = 0; i < ptsArr.size(); i++)
		{
			for (int j = 0; j < ptsArr[i].size(); j++)
			{
				_x = ptsArr[i][j].coordinate[0] + mx;
				_y = ptsArr[i][j].coordinate[1] + my;
				_z = ptsArr[i][j].coordinate[2] + mz;

				_x_r = (x_rot_1 * _x) + (x_rot_2 * _y) - (x_rot_3 * _z);
				_y_r = y_rot_1 * _x + y_rot_2 * _y + y_rot_3 * _z;
				_z_r = z_rot_1 * _x + z_rot_2 * _y + z_rot_3 * _z;

				/*tx = texturesArr[i][j].coordinate[0] + mx;
				ty = texturesArr[i][j].coordinate[1] + my;
				tz = texturesArr[i][j].coordinate[2] + mz;

				tx_r = (x_rot_1 * tx) + (x_rot_2 * ty) - (x_rot_3 * tz);
				ty_r = y_rot_1 * tx + y_rot_2 * ty + y_rot_3 * tz;
				tz_r = z_rot_1 * tx + z_rot_2 * ty + z_rot_3 * tz;*/

				nx = ptsArr[i][j].normalComb[0];
				ny = ptsArr[i][j].normalComb[1];
				nz = ptsArr[i][j].normalComb[2];

				nx_r = (x_rot_1 * nx) + (x_rot_2 * ny) - (x_rot_3 * nz);
				ny_r = y_rot_1 * nx + y_rot_2 * ny + y_rot_3 * nz;
				nz_r = z_rot_1 * nx + z_rot_2 * ny + z_rot_3 * nz;

				/*nx_r /= ptsArr[i][j].normal_x.size();
				ny_r /= ptsArr[i][j].normal_y.size();
				nz_r /= ptsArr[i][j].normal_z.size();*/

				modifiedPts[i][j].Set(_x_r, _y_r, _z_r);
				//modifiedTextures[i][j].Set(tx_r, ty_r, tz_r);
				modifiedPts[i][j].SetNormalComb(nx_r, ny_r, nz_r);
			}
		}
	}
	Vertex GetCenterPosition(Camera &camera)
	{
		float _x, _y, _z, _x_r, _y_r, _z_r;
		float mx, my, mz;
		mx = x + (-1.0) * camera.position.coordinate[0];
		my = y + (-1.0) * camera.position.coordinate[1];
		mz = z + (-1.0) * camera.position.coordinate[2];

		float x_rot_1 = cos(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[2]);
		float x_rot_2 = cos(camera.rotation.coordinate[1]) * sin(camera.rotation.coordinate[2]);
		float x_rot_3 = sin(camera.rotation.coordinate[1]);

		float y_rot_1 = ((sin(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[2])) - (cos(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[2])));
		float y_rot_2 = ((sin(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * sin(camera.rotation.coordinate[2])) + (cos(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[2])));
		float y_rot_3 = (sin(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[1]));

		float z_rot_1 = ((cos(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[2])) + (sin(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[2])));
		float z_rot_2 = ((cos(camera.rotation.coordinate[0]) * sin(camera.rotation.coordinate[1]) * sin(camera.rotation.coordinate[2])) - (sin(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[2])));
		float z_rot_3 = (cos(camera.rotation.coordinate[0]) * cos(camera.rotation.coordinate[1]));

		_x = mx;
		_y = my;
		_z = mz;

		_x_r = (x_rot_1 * _x) + (x_rot_2 * _y) - (x_rot_3 * _z);
		_y_r = y_rot_1 * _x + y_rot_2 * _y + y_rot_3 * _z;
		_z_r = z_rot_1 * _x + z_rot_2 * _y + z_rot_3 * _z;

		return Vertex(_x_r, _y_r, _z_r);
	}
};

Mesh SirKettle(0, 0, -1);
Mesh Tree(-1, 0.2, -4);
Mesh Sol(-4, 4, -5);
Mesh Umbreon(-0.5, 0.15, -4.6);
Mesh Groudon(-0.30, 0.55, -4.4);
Camera camera(0, 0, 0);
int mode = 1;

void DoInit()
{
	//glOrtho(-Size, Size, -Size, Size, -Size, Size);
	glMatrixMode(GL_PROJECTION);
	//glViewport(0, 0, 0, 0);
	glLoadIdentity();
	gluPerspective(90, 1, 0.01, 100);
	glEnable (GL_DEPTH_TEST);
}

void Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	if (mode == 1)
	{
		SirKettle.GouraudShading(camera);
		Tree.GouraudShading(camera);
		Sol.GouraudShading(camera);
		Umbreon.GouraudShading(camera);
		Groudon.GouraudShading(camera);
	}
	else if (mode == -1)
	{
		SirKettle.FlatShading(camera);
		Tree.FlatShading(camera);
		Sol.FlatShading(camera);
		Umbreon.FlatShading(camera);
		Groudon.FlatShading(camera);
	}
	
	glutSwapBuffers();
}

void UpdateObjectsPosition()
{
	SirKettle.Update(camera);
	Tree.Update(camera);
	Sol.Update(camera);
	Umbreon.Update(camera);
	Groudon.Update(camera);
	camera.UpdateLight(Sol.GetCenterPosition(camera));
}

void Idle()
{
	//glRotatef(1, -1, 1, 0);
	glutPostRedisplay();
}

void KeyInput(unsigned char key, int x, int y)
{
	float scale = movementScale;
	if (key == 'a')
	{
		camera.Translate(-1.0 * scale * cos(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[0]), 0, 1.0 * scale * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[0]));
	}
	else if (key == 'd')
	{
		camera.Translate(1.0 * scale * cos(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[0]), 0, -1.0 * scale * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[0]));
	}
	else if (key == 'w')
	{
		camera.Translate(-1.0 * scale * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[0]), 1.0 * scale * sin(camera.rotation.coordinate[0]), -1.0 * scale * cos(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[0]));
	}
	else if (key == 's')
	{
		camera.Translate(1.0 * scale * sin(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[0]), -1.0 * scale * sin(camera.rotation.coordinate[0]), 1.0 * scale * cos(camera.rotation.coordinate[1]) * cos(camera.rotation.coordinate[0]));
	}
	else if (key == char(32))
	{
		camera.Translate(0, 1.0 * scale, 0);
	}
	else if (key == 'b')
	{
		camera.Translate(0, -1.0 * scale, 0);
	}
	else if (key == 'h')
	{
		camera.Rotate(0, 10 * scale, 0);
	}
	else if (key == 'k')
	{
		camera.Rotate(0, -10 * scale, 0);
	}
	else if (key == 'u')
	{
		camera.Rotate(10 * scale, 0, 0);
	}
	else if (key == 'j')
	{
		camera.Rotate(-10 * scale, 0, 0);
	}
	else if (key == 'i')
	{
		camera.Rotate(0, 0, 90 * scale);
	}
	else if (key == 'o')
	{
		camera.Rotate(0, 0, -90 * scale);
	}
	else if (key == 'p')
	{
		mode *= -1;
	}
	UpdateObjectsPosition();
	glutPostRedisplay();
}

void InitializeObjects()
{
	SirKettle.LoadFromFile("Objects/House/WoodHouse.obj");
	SirKettle.SetScale(house_scale);
	SirKettle.CalculatePosition(camera);
	Tree.LoadFromFile("Objects/Decorations/Tree_Low_Poly.obj");
	Tree.SetScale(tree_scale);
	Tree.CalculatePosition(camera);
	Sol.LoadFromFile("Objects/Decorations/Sol.obj");
	Sol.SetScale(sol_scale);
	Sol.CalculatePosition(camera);
	Umbreon.LoadFromFile("Objects/Decorations/Umbreon_Low_Poly.obj");
	Umbreon.SetScale(umbreon_scale);
	Umbreon.CalculatePosition(camera);
	Groudon.LoadFromFile("Objects/Decorations/Groudon.obj");
	Groudon.SetScale(groudon_scale);
	Groudon.CalculatePosition(camera);

	camera.UpdateLight(Sol.GetCenterPosition(camera));
}

void main(int argc, char **argv)
{
	InitializeObjects();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	
	glutInitWindowSize(800,600);
	glutCreateWindow("OpenGL");
	glutDisplayFunc(Render);
	DoInit();
	//glutMouseFunc(MouseInput);
	glutKeyboardFunc(KeyInput);
	//glutMotionFunc(HoldUpdate);
	glutIdleFunc(Idle);
	glutMainLoop();
}